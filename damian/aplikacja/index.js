const express = require("express");
const routes = require("./routes/admin");
const pages = require("./routes/pages")
const files = require("./routes/files")

const app = express();

// app.use('/admin', routes)
// app.use('/pages', pages)

app.use(express.urlencoded({}))
app.use(express.json({}))


app.set('view engine', 'ejs');
app.set('views', './views')
// https://ejs.co/#docs
app.set('view options', {});

app.use(function (req, res, next) {
    console.log('Time:', new Date().toLocaleTimeString())
    console.log('Request Type:', req.method)
    console.log(req.originalUrl)
    next()
  })

app.use('/admin', routes)
app.use('/pages', pages)
app.use('/files', files)




app.get("/", function (req, res) {
    res.status(200).redirect('/pages')
        // .send("<h1>GET request to homepage</h1>");
});

app.get("/api", function (req, res) {
    res.status(200)
        .send({ message: 'Hello API' });
});

app.post("/api/users/signin", function (req, res) {
    debugger
    res.status(200)
        .send({ message: 'Hello API' });
});

const server = app.listen(3000);
