
/* TODO:
    - Home page html
    - Signin page html
    - Signup page html
    - Conctact page html
*/

var { Router } = require("express");

// const routers= Router()

const routes= Router()

// routers.get("/", function (req, res) {
//     res.status(200).send(`<h1>Home page</h1>`);
// });
// routers.get("/Signin", function (req, res) {
//     res.status(200).send(`<h1>Signin</h1>`);
// });
// routers.get("/Signup", function (req, res) {
//     res.status(200).send(`<h1>Signup</h1>`);
// });
// routers.get("/Contact", function (req, res) {
//     res.status(200).send(`<h1>Contact</h1>`);
// });


// routes.get("/", function (req, res) {
//     res.send(/* html */`
//     <div class="container">
//         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
//         <a class="nav-link float-end" href="/pages/">Back</a>
//         <h3 class="display-4 mt-3">Welcome ${'Admin'}</h3>

//         <ul class="nav mb-3 d-flex">
//             <li class="nav-item">
//                 <a class="nav-link" href="/pages/contact">Contact</a>
//             </li>
//             <li class="w-auto"></li>
//             <li class="nav-item">
//                 <a class="nav-link" href="/pages/signin">Sign In</a>
//             </li>
//             <li class="nav-item">
//                 <a class="nav-link" href="/pages/signup">Sign Up</a>
//             </li>
//         </li>
//     </div>`)
// });

// routes.get("/signin", function (req, res) {
//     res.send(/* html */`
//     <div class="container">
//         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
//         <a class="nav-link float-end" href="/pages/">Back</a>
//         <h1 class="display-1 mt-3">Sign In</h1>
//         <form method>
//             <div class="form-group mt-3">
//               <label for="email">E-mail</label>
//               <input type="text"
//                 class="form-control" name="email" id="email" placeholder="E-mail">
//             </div>
//             <div class="form-group mt-3">
//               <label for="password">Password</label>
//               <input type="password"
//                 class="form-control" name="password" id="password" placeholder="Password">
//             </div>
//             <button type="submit" class="btn btn-primary mt-4">Submit</button>
//         </form>    
//     </div>
//     `);
// });

// routes.get("/signup", function (req, res) {
//     res.send(/* html */`
//     <div class="container">
//         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
//         <a class="nav-link float-end" href="/pages/">Back</a>
//         <h1 class="display-1 mt-3">Sign Up</h1>
//         <form>
//              <div class="form-group mt-3">
//               <label for="email">E-mail</label>
//               <input type="text"
//                 class="form-control" name="email" id="email" placeholder="E-mail">
//             </div>
//             <div class="form-group mt-3">
//               <label for="password">Password</label>
//               <input type="password"
//                 class="form-control" name="password" id="password" placeholder="Password">
//             </div>
//             <button type="submit" class="btn btn-primary mt-4">Submit</button>
//         </form>  
//     </div>   
// `);
// });

// routes.get("/contact", function (req, res) {
//     // const email = req.query.email
//     // const { email, message } = req.query 
//     const { email = '', message = '' } = req.query 

//     debugger
    
//     res.send(/* html */`
//     <div class="container">
//         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
//         <h1 class="display-1 mt-3">Contact Us</h1>
//         <form method="GET" action="${req.baseUrl}/contact">
//              <div class="form-group mt-3">
//               <label for="email">E-mail</label>
//               <input type="text"
//                 class="form-control" name="email" id="email" placeholder="E-mail" value="${email}">
//             </div>
//             <div class="form-group mt-3">
//               <label for="message">Message</label>
//               <textarea class="form-control" name="message" id="message" placeholder="Write something here">${message}</textarea>
//             </div>
//             <button type="submit" class="btn btn-primary mt-4">Submit</button>
//         </form>    
//     </div>   
// `);
// });

routes.use(function (req, res, next) {
    req.user = { name: 'Guest' }
    // throw (new Error('Cant load user'))
    // next(new Error('Cant load user'))
    next()
})

routes.get("/", function (req, res) {
    res.render('pages/index')
});

routes.get("/signin", function (req, res) {
    res.render('pages/signin')

});

routes.get("/signup", function (req, res) {
    res.render('pages/signup')
});

routes.get("/contact", function (req, res) {
    const { email = '', message = '' } = req.query
    res.render('pages/contact', { req, email :'', message:'' })
});
routes.post("/contact", function (req, res) {
    const { email = '', message = '' } = req.query
    res.render('pages/contact', { req, email, message })
});

routes.get("/about", function (req, res) {
    res.render('pages/about')
});


module.exports = routes


// module.exports = routers