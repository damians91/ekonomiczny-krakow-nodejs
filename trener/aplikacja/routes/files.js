const { Router } = require("express");

const fs = require('fs')
const path = require('path');
const { promisify } = require("util");


const routes = Router()

function readProductData(dir, callback) {
    fs.readFile(path.join(dir, 'product.json'), (err, fileData) => {
        if (err) { callback(err); return; }
        const data = JSON.parse(fileData);
        const product = {
            id: data.id,
            name: data.name,
        };
        // read description.md
        fs.readFile(path.join(dir, 'description.md'), (err, mdData) => {
            if (err) { callback(err); return; }

            // - description
            product.description = mdData.toString().substring(0, 20);

            callback(null, product)
        });
    });
}

routes.get('/generate/catalog', (req, res) => {

    const productsDir = '../../dane/products/';
    const products = []
    let currentDirIndex = 0;

    // list folders
    fs.readdir(productsDir, (err, dirs) => {
        if (err) { console.error(err); return; }

        const callback = (err, product) => {
            if (err) { console.error(err); return; }

            products.push(product);

            // Get next product
            const nextDir = dirs[++currentDirIndex];
            // If next continue
            if (nextDir) {
                const dir = path.join(productsDir, nextDir)
                readProductData(dir, callback)

                // If last - break recucrence and finish
            } else {
                // Save Catalog to 
                fs.writeFile('public/products-catalog.json', JSON.stringify(products, null, 2), (err) => {
                    if (err) { console.error(err); return; }
                    res.json(products);
                });
            }
        }
        // First Product:
        readProductData(path.join(productsDir, dirs[currentDirIndex]), callback)
    })
})

/* 
    fetch('http://localhost:3000/files/',{})
    .then(res => res.json())
    .then(console.log)
*/

// const readdirAsync = promisify(fs.readdir)
// readdirAsync(dir).then()

routes.get('/list/:directory?', async (req, res) => {

    try {
        const dirPath = path.join('./public', req.params.directory || '');
        const results = []
        for await (file of readDirAsyncGen(dirPath)) {
            results.push(file)
            // await ...
        }
        res.json(results)

    } catch (err) {
        return res.status(500).send(err)
    }

    async function* readDirAsyncGen(dirPath) {
        const dirsIterator = await fs.promises.opendir(dirPath);

        for await (const dirent of dirsIterator) {
            if (dirent.isDirectory()) {
                // for await (file of readDirAsyncGen(path.join(dirPath, dir))) {
                //     yield (file)
                // }
                yield* readDirAsyncGen(path.join(dirPath, dirent.name))
            } else {
                yield path.join(dirPath, dirent.name)
            }
        }
    }
})

routes.get('/list2/:directory?', async (req, res) => {

    try {
        const dirPath = path.join('./public', req.params.directory || '');
        const results = await readDirRecursive(dirPath);
        res.json(results)

    } catch (err) {
        return res.status(500).send(err)
    }

    async function readDirRecursive(dirPath) {
        const files = await fs.promises.readdir(dirPath, { withFileTypes: true });
        const results = []

        for (const file of files) {
            if (file.isDirectory()) {
                const children = await readDirRecursive(path.join(dirPath, file.name));
                results.push(...children)
            } else {
                results.push(path.join(dirPath, file.name));
            }
        }
        return results
    }
})

/* 
    fetch('http://localhost:3000/files/text copy.txt',{
        method:'DELETE'
    })
    .then(res => res.json())
    .then(console.log)
*/

routes.delete('/:fileName', (req, res) => {

    const filePath = path.resolve('./public', req.params.fileName);
    fs.unlink(filePath, (err) => {
        if (err) {
            return res.status(500).send(err)
        }
        res.json({ message: 'ok' })
    })

})


routes.get('/:fileName', (req, res) => {
    // TODO: send file
})


module.exports = routes


