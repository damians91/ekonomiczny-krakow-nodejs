
var { Router } = require("express-promise-router");
const users = require("../../model/users");

const routes = Router()

// GET api/users/
routes.get("/", async (req, res, next) => {
    // express-promise-router
    // try {
    res.json(await users.getAll());
    // } catch (err) {
    //  next(err)
    // }
});

// GET api/users/123
routes.get("/:id", async (req, res) => {
    res.json(await users.findById(req.params.id));
});

// GET api/users/123/posts
// routes.get("/:id/posts", async (req, res) => {
//     res.json({ id: '31', title: 'Test post' });
// });

// POST api/users/ {...}
routes.post("/", async (req, res) => {
    const draft = req.body;
    /// todo: validate 
    res.json(await users.create(draft));
});

// PUT api/users/123 {...}
routes.put("/:id", async (req, res) => {
    const draft = req.body;
    /// todo: validate 
    res.json(await users.update(draft));
});

// DELETE api/users/123
routes.delete("/:id", async (req, res) => {
    await users.delete(req.params.id)

    res.json({ deleted: req.params.id });
});


module.exports = routes