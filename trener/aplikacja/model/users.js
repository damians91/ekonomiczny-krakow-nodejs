

class UsersRepository {

    users = [{ id: '123', name: 'testuser' }]

    async getAll() {
        return this.users
    }

    async findById(id) { }
    async create(draft) { }
    async update(draft) { }
    async delete(id) { }
}


module.exports = new UsersRepository()